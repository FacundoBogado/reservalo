﻿$(document).ready(function () {

    $('#Dropdown').click(function () {
        $('#Menu').toggleClass('show');
        $('#Submenu').hide();
    });

    $('#Proyectos').click(function (e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });
});