﻿$(document).ready(function () {

    esconderCampos();

    $('#PopUp').click(function () {

        var nombre = $('#Nombre').val();
        var apellido = $('#Apellido').val();
        var usuario = $('#NombreDeUsuario').val();
        var email = $('#EMail').val();
        var contrasena = $('#Contrasena').val();
        var confirmarContrasena = $('#ConfirmarContrasena').val();
        var pais = $('#Pais').val();
        var genero = $('#Genero').val();
        var fecha = $('#FechaDeNacimiento').val();

        if (nombre != "", apellido != "", usuario != "", email != "", contrasena != "", confirmarContrasena != "", pais != "", genero != "", fecha != "") {

            $.post("ValidarCampos", { email, usuario, contrasena, confirmarContrasena }, function (data) {

                if (data == 1) {
                    $('#Modal').css("display", "block");
                    $('.Contenido').show();
                } else if (data == 2) {

                    alert("El Email ya esta en uso elige otro por favor");

                } else if (data == 3) {

                    alert("El nombre de usuario esta en uso elige otro por favor");

                } else if (data == 0) {

                    alert("Las contraseñas no coinciden");

                } else {
                    alert("Hubo un error al procesar sus datos intentelo de nuevo mas tarde.")
                }

            });

        } else {

            alert("Faltan datos por completar.");
        }
    });

});

function callback() {

    console.log("El usuario completo el captcha! ")

}

function esconderCampos() {
    $('#Modal').hide();
    $('.contenido').hide();
    $('#CodigoVerificacion').hide();
}