﻿using Reservalo.Models;
using Reservalo.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Reservalo.Controllers;

namespace Reservalo.Controllers
{
    public class CuentaController : Controller
    {
        ReservaloContext db = new ReservaloContext();
        // GET: Cuenta
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(IniciarSesionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var validarUser = db.Usuario.FirstOrDefault(x => x.NombreDeUsuario == model.NombreDeUsuario && x.Contrasena == model.Contrasena);

                if (validarUser != null)
                {
                    //SerializeObject: es capas de leer y entender json
                    var json = JsonConvert.SerializeObject(new
                    {

                        validarUser.NombreDeUsuario,
                        validarUser.UsuarioId,
                    }
                    );

                    //Crear cookie
                    var UsuariosCookie = new HttpCookie("Usuario", model.NombreDeUsuario);
                    var IdUsuario = new HttpCookie("UsuarioId", validarUser.UsuarioId.ToString());

                    //seteo datos a cookie
                    HttpContext.Response.SetCookie(UsuariosCookie);
                    HttpContext.Response.SetCookie(IdUsuario);

                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("Error", "Los datos ingresados no coinciden");
                return View(model);
            }

            return View(model);
        }

        public ActionResult Logout()
        {
            if (Request.Cookies["Usuario"] != null)
            {
                var usuario = new HttpCookie("Usuario")
                {
                    Expires = DateTime.Now.AddDays(-1),
                    Value = null
                };
                Response.SetCookie(usuario);
            }

            return RedirectToAction("Index", "Home");

        }

        [HttpPost]
        public async Task<ActionResult> ResetPassword(ForgotPassword model)
        {
            if (ModelState.IsValid)
            {
                var validarMail = db.Usuario.FirstOrDefault(x => x.EMail == model.EMail);
                var email = model.EMail;
                if (validarMail == null)
                {
                    ModelState.AddModelError("NoExite", "El mail ingresado es incorrecto o no existe");
                    return View(model);
                }

                var usuario = new UsuarioController();

                usuario.ResetPass(email);

                return View("ResetPasswordConfirmation");
            }

            //Si se llega hasta aca hay errores
            return View(model);
        }

        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        public ActionResult ResetPasswordConfirmed(NuevaContraseña model)
        {
            if (ModelState.IsValid)
            {
                if (model.Contrasena != model.ConfirmarContrasena)
                {
                    ModelState.AddModelError("Diferencia", "Las contraseñas no coinciden!");
                    return View(model);
                }

                Usuario usuario = db.Usuario.FirstOrDefault(u => u.EMail == model.Email);

                if (usuario != null)
                {
                    usuario.Contrasena = model.Contrasena;
                    usuario.ConfirmarContrasena = model.ConfirmarContrasena;

                    db.Entry(usuario).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("noExiste", "El mail no es correcto o no esta registrado");
                return View(model);

            }

            //Si se llega hasta aca hay errores
            return View();
        }
    }
}