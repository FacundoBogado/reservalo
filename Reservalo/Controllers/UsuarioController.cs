﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Reservalo.Models;
using Reservalo.ViewModel;

namespace Reservalo.Controllers
{
    public class UsuarioController : Controller
    {
        private ReservaloContext db = new ReservaloContext();

        // GET: Usuarios
        public ActionResult Index()
        {
            return View(db.Usuario.ToList());
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Usuarios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UsuarioViewModel usuario)
        {
            if (!usuario.CodigoVerificacion.Equals(Session["guid"]))
            {
                ModelState.AddModelError("CodigoIncorrecto", "El codigo ingresado no es el correcto por favor intente de nuevo");
                return View(usuario);
            }

            if (ModelState.IsValid)
            {
                var registrar = new Usuario();
                {
                    registrar.CodigoVerificacion = usuario.CodigoVerificacion;
                    registrar.Apellido = usuario.Apellido;
                    registrar.Contrasena = usuario.Contrasena;
                    registrar.ConfirmarContrasena = usuario.ConfirmarContrasena;
                    registrar.EMail = usuario.EMail;
                    registrar.FechaDeNacimiento = usuario.FechaDeNacimiento;
                    registrar.Genero = usuario.Genero;
                    registrar.Nombre = usuario.Nombre;
                    registrar.NombreDeUsuario = usuario.NombreDeUsuario;
                    registrar.UsuarioId = usuario.UsuarioId;
                    registrar.Pais = usuario.Pais;

                };

                db.Usuario.Add(registrar);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");

            }

            return View(usuario);
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuarios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Exclude = "FotoDeUsuario")]Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                var usuarioId = Request.Cookies["UsuarioId"].Value;
                var id = Int32.Parse(usuarioId);
                var usuarioEditado = db.Usuario.Find(id);
                if (usuarioEditado == null)
                {
                    return HttpNotFound();
                }

                if (usuario.CambiarFoto == true)
                {
                    byte[] imagen = null;
                    if (Request.Files.Count > 0)
                    {
                        HttpPostedFileBase foto = Request.Files["FotoDeUsuario"];
                        if (foto.ContentLength > 0)
                        {
                            using (var binary = new BinaryReader(foto.InputStream))
                            {
                                imagen = binary.ReadBytes(foto.ContentLength);
                            }

                        }
                        else
                        {
                            string archivoPredeterminado = HttpContext.Server.MapPath(@"~/Imagenes/avatar-inside-a-circle.png");
                            FileInfo info = new FileInfo(archivoPredeterminado);
                            long largoDeImagen = info.Length;
                            FileStream fs = new FileStream(archivoPredeterminado, FileMode.Open, FileAccess.Read);
                            BinaryReader br = new BinaryReader(fs);
                            imagen = br.ReadBytes((int)largoDeImagen);
                        }
                    }

                    usuarioEditado.FotoDeUsuario = usuario.FotoDeUsuario = imagen;
                }

                if (usuario.Contrasena != null && usuario.ConfirmarContrasena != "")
                {
                    usuarioEditado.Contrasena = usuario.Contrasena;
                    usuarioEditado.ConfirmarContrasena = usuario.ConfirmarContrasena;
                }

                usuarioEditado.UsuarioId = id;
                usuarioEditado.EMail = usuario.EMail;


                db.Entry(usuarioEditado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("index", "Home");

            }

            return View(usuario);
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario usuario = db.Usuario.Find(id);
            Logout();
            db.Usuario.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            if (Request.Cookies["Usuario"] != null)
            {
                var usuario = new HttpCookie("Usuario")
                {
                    Expires = DateTime.Now.AddDays(-1),
                    Value = null
                };
                Response.SetCookie(usuario);
            }

            return RedirectToAction("Index", "Home");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public JsonResult ValidarCampos(string email, string usuario, string contrasena, string confirmarContrasena, string usuarioId)
        {
            var validarUserName = db.Usuario.FirstOrDefault(x => x.NombreDeUsuario == usuario);
            var validarMail = db.Usuario.FirstOrDefault(x => x.EMail == email);
            var validarUsuarioId = db.Usuario.FirstOrDefault(x => x.UsuarioId.ToString() == usuarioId);

            if (contrasena != confirmarContrasena)
            {
                return Json(0);
            }

            if (validarMail != null && validarUsuarioId == null)
            {
                return Json(2);

            }

            if (validarUserName != null && validarUsuarioId == null)
            {
                return Json(3);
            }

            MailDeConfirmacion(email);

            return Json(1);
        }

        public static void MailDeConfirmacion(string email)
        {
            var codigo = Guid.NewGuid().ToString();
            string cuerpo = System.IO.File.ReadAllText(HostingEnvironment.MapPath("/Views/Mail/Texto.cshtml"));
            cuerpo = cuerpo.Replace("valor", codigo);
            ModeloDeMail("Tu cuenta ha sido creada!", cuerpo, email);
            System.Web.HttpContext.Current.Session["guid"] = codigo;

        }

        public void ResetPass(string email)
        {
            string cuerpo = System.IO.File.ReadAllText(HostingEnvironment.MapPath("/Views/Mail/ResetPass.cshtml"));
            ModeloDeMail("Reinicio de contraseña", cuerpo, email);

        }

        public static void ModeloDeMail(string asunto, string cuerpo, string email)
        {
            string de, para, bcc, cc, subject, body;
            de = "equipodereservalo@gmail.com";
            para = email.Trim();
            bcc = "";
            cc = "";
            subject = asunto;
            StringBuilder texto = new StringBuilder();
            texto.Append(cuerpo);
            body = texto.ToString();
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(de);
            mail.To.Add(new MailAddress(para));
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            EnviarEmail(mail);
        }

        public static bool EnviarEmail(MailMessage mail)
        {

            SmtpClient cliente = new SmtpClient();

            cliente.Host = "smtp.gmail.com";
            cliente.Port = 587;
            cliente.EnableSsl = true;
            cliente.UseDefaultCredentials = false;
            cliente.DeliveryMethod = SmtpDeliveryMethod.Network;
            cliente.Credentials = new System.Net.NetworkCredential("equipodereservalo@gmail.com", "Atr1234567");

            try
            {
                cliente.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        [HttpPost]
        public JsonResult ValidarMail(string email)
        {
            var usuarioId = Request.Cookies["UsuarioId"];
            var validarMail = db.Usuario.FirstOrDefault(x => x.EMail == email);
            //var validarUserName = db.Usuarios.FirstOrDefault(x => x.NombreDeUsuario == usuario);
            //var validarUsuarioId = db.Usuarios.FirstOrDefault(x => x.UsuarioId.ToString() == usuarioId.Value);

            if (validarMail != null)
            {
                return Json(2);

            }

            //if (validarUserName != null && validarMail == null)
            //{
            //    return Json(3);
            //}

            return Json(1);
        }

        [HttpPost]
        public JsonResult ValidarUsuario(string usuario)
        {
            var usuarioId = Request.Cookies["UsuarioId"];
            var validarUserName = db.Usuario.FirstOrDefault(x => x.NombreDeUsuario == usuario);
            //var validarUsuarioId = db.Usuarios.FirstOrDefault(x => x.UsuarioId.ToString() == usuarioId.Value);

            if (validarUserName != null)
            {
                return Json(3);
            }

            return Json(1);
        }

        public FileContentResult FotoDePerfil()
        {
            byte[] imagen = null;
            var usuario = Request.Cookies["UsuarioId"];
            //cambiar esto y preguntar si tiene foto

            if (usuario != null)
            {

                imagen = db.Usuario.FirstOrDefault(i => i.UsuarioId.ToString() == usuario.Value).FotoDeUsuario;

                if (imagen != null)
                {

                    return File(imagen, "image/png");

                }
            }

            string archivoPredeterminado = HttpContext.Server.MapPath(@"~/Imagenes/avatar-inside-a-circle.png");
            FileInfo info = new FileInfo(archivoPredeterminado);
            long largoDeImagen = info.Length;
            FileStream fs = new FileStream(archivoPredeterminado, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            imagen = br.ReadBytes((int)largoDeImagen);

            return File(imagen, "image/png");
        }
    }
}
