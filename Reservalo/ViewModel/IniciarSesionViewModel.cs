﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Reservalo.ViewModel
{
    public class IniciarSesionViewModel
    {
        [Required(ErrorMessage = "El campo Nombre de Usuario es requerido")]
        [Display(Name = "Nombre de Usuario")]
        public string NombreDeUsuario { get; set; }

        [Required(ErrorMessage = "El campo Contraseña es requerido")]
        [Display(Name = "Contraseña")]
        public string Contrasena { get; set; }
    }
}