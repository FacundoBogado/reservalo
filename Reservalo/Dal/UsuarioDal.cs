﻿using Reservalo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reservalo.Dal
{
    public class UsuarioDal
    {
        public ReservaloContext contexto = new ReservaloContext();

        public void RegistrarUsuario(Usuario registrar)
        {
            try
            {
                contexto.Usuario.Add(registrar);
                contexto.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}