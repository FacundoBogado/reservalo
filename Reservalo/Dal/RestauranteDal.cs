﻿using Reservalo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reservalo.Dal
{
    public class RestauranteDal
    {
        public ReservaloContext contexto = new ReservaloContext();

        public void RegistrarUsuario(Restaurante restaurante)
        {
            try
            {
                contexto.Restaurantes.Add(restaurante);
                contexto.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}