﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reservalo.Models
{
    public class ValidacionCaptcha
    {
        [JsonProperty("Success")]
        public bool Exito
        {
            get;
            set;
        }
        [JsonProperty("Error")]
        public List<string> MensajeError
        {
            get;
            set;
        }
    }
}