﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Reservalo.Models
{
    public class Usuario
    {
        public int UsuarioId { get; set; }

        [Required(ErrorMessage = "El campo Nombre es requerido")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo Apellido es requerido")]
        public string Apellido { get; set; }

        [Required(ErrorMessage = "El campo E-Mail es requerido")]
        [Display(Name = "E-Mail")]
        public string EMail { get; set; }

        [Required(ErrorMessage = "El campo Contraseña es requerido")]
        [Display(Name = "Contraseña ")]
        public string Contrasena { get; set; }

        [Required(ErrorMessage = "El campo Confirmar Contraseña es requerido")]
        [Display(Name = "Confirmar contraseña   ")]
        public string ConfirmarContrasena { get; set; }

        [Required(ErrorMessage = "El campo Pais es requerido")]
        public string Pais { get; set; }

        [Required(ErrorMessage = "El campo Genero es requerido")]
        public string Genero { get; set; }

        [Required(ErrorMessage = "El campo Fecha de Nacimiento es requerido")]
        [Display(Name = "Fecha de nacimiento")]
        public DateTime FechaDeNacimiento { get; set; }

        [Required(ErrorMessage = "El campo Nombre de Usuario es requerido")]
        [Display(Name = "Nombre de Usuario")]
        public string NombreDeUsuario { get; set; }

        public string CodigoVerificacion { get; set; }

        [Display(Name = "Foto de Perfil")]
        public byte[] FotoDeUsuario { get; set; }

        public bool CambiarFoto { get; set; }

        public bool Pago { get; set; }
    }
}