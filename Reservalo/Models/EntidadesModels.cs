﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Reservalo.Models
{
    public class ReservaloContext : DbContext
    {
        public DbSet<Usuario> Usuario { get; set; }

        public DbSet<Restaurante> Restaurantes { get; set; }

    }
}