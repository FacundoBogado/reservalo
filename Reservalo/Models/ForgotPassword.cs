﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Reservalo.Models
{
    public class ForgotPassword
    {
        [Required(ErrorMessage = "El Email es requerido")]
        [EmailAddress]
        public string EMail { get; set; }
    }

    public class NuevaContraseña
    {
        [Required(ErrorMessage = "El campo Contraseña es requerido")]
        public string Contrasena { get; set; }

        [Required(ErrorMessage = "El campo Confirmar contraseña es requerido")]
        public string ConfirmarContrasena { get; set; }

        [Required(ErrorMessage = "El campo Email es requerido")]
        public string Email { get; set; }

    }
}