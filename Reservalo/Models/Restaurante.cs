﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Reservalo.Models
{
    public class Restaurante
    {
        public int RestauranteId { get; set; }

        [Required(ErrorMessage = "El campo Nombre es requerido")]
        public String Nombre { get; set; }

        [Required(ErrorMessage = "El campo Descripcion es requerido")]
        public String Descripcion { get; set; }

        [Required(ErrorMessage = "El campo Ubicacion es requerido")]
        public String Ubicacion { get; set; }
    }
}