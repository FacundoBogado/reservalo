﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Reservalo.Startup))]
namespace Reservalo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
